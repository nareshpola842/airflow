from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
from airflow.utils.dates import days_ago
from airflow.operators.email_operator import EmailOperator
from airflow.hooks.mysql_hook import MySqlHook
import itertools
from airflow.models import Variable
import pandas as pd
from python import *
#from  pretty_html_table import build_table
from tabulate import tabulate


def fetch_variables():
  settings = Variable.get("mysq", deserialize_json=True)
  print(settings)
  for st in settings:
      print(st)
      print(settings[st])
      fetch_records(st,settings[st])

def fetch_records(mysqlid,maxvalue):
     request = "select substring_index(HOST,':',1) Host from information_schema.processlist group by 1 having count(*)> "+maxvalue
     print("Query ",request)
     mysql_hook = MySqlHook(mysql_conn_id = mysqlid, schema = 'mysql')
     try:
         connection = mysql_hook.get_conn()
         print(connection)
     except Exception:
         print("Exception ")
     else:
         cursor = connection.cursor()
         print(cursor)
         cursor.execute(request)
         sources = cursor.fetchall()
         print(sources)





task = PythonOperator(
  task_id = 'fetch_variables',dag=dag,python_callable = fetch_variables)





task
