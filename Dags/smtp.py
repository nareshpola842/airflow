import smtplib,ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from airflow.models import Variable
from email.message import EmailMessage
from email.mime.base import MIMEBase
import os
from email import encoders

def email_send(text,host):
      value = Variable.get("email",deserialize_json=True)
      for username in value:
            sender_mail=username
      password = value[username]
      receiver_mail = "yourmail@gmail.com"
      message = MIMEMultipart("multipart")
      message["From"] = "donotreply<yourmail>"
      message["To"] = receiver_mail
      message["Subject"] = "IP Addresses"

      sub="\nHi,\nAttached files have database reports. Please check.\nThanks\n"
      text1 = MIMEText(sub,"plain")
      message.attach(text1)

      with open("text1.txt",'w') as file:
          file.writelines([f"\noutput of Host:{host}\n",text])



      attachment = MIMEBase('application', 'octet-stream')
      attachment.set_payload(open("text1.txt", 'rb').read())
      encoders.encode_base64(attachment)
      attachment.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename("IP_NB.txt"))
      message.attach(attachment)
      message = message.as_string()
      server = smtplib.SMTP(host = "smtp.gmail.com",port = 587)
      server.ehlo()
      server.starttls()
      server.login(sender_mail,password)
      server.sendmail(sender_mail,receiver_mail,message)
      print("sending mail...")
#def send_format(max,host,net,*lst):
#      format = "Hi,\n\n        We are observing below services are having more than {} connections in Host of {} of {} Network Below is the processlist \n \n{}".format(max,host,net,lst)
#      email_send(format)
