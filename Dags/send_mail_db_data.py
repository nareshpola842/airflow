from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.models import Variable
from airflow.hooks.mysql_hook import MySqlHook
from airflow.utils.dates import days_ago
import itertools
from python3 import *
from tabulate import tabulate
import pandas as pd

def fetch_variables():
  settings = Variable.get("mysq_db_args", deserialize_json=True)
  print(settings)
  for st in settings:
      print(st)
      print(settings[st])
      fetch_records(st,settings[st])

def fetch_records(mysqlid,maxvalue):
  request = "select substring_index(HOST,':',1) Host from information_schema.processlist group by 1 having count(*)> "+maxvalue
  print("Query ",request)
  mysql_hook = MySqlHook(mysql_conn_id = mysqlid, schema = 'mysql')
  try:
      connection = mysql_hook.get_conn()
      print(connection)
  except Exception:
      print("Exception ")
  else:
      cursor = connection.cursor()
      print(cursor)
      cursor.execute(request)
      sources = cursor.fetchall()
      data=list(itertools.chain(*sources))

      conn = MySqlHook.get_connection(mysqlid)
      Host = conn.host
      net=Variable.get("network",deserialize_json=True)
      send_format(maxvalue,Host,net,data)
default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": days_ago(1),
    "retries": 0,
    "email":["examplem39@gmail.com"],
    "email_on_failure":True,
    "email_on_retry":False}

dag = DAG("DB_APPLICATION", default_args=default_args,catchup=False,schedule_interval='*/1 * * * *')

task=PythonOperator(task_id='task',dag=dag,python_callable=fetch_variables)


task
