from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from airflow.operators.trigger_dagrun import TriggerDagRunOperator


from datetime import datetime

default_args = {
    'start_date': days_ago(1)
}

def _downloading():
    print('downloading')

with DAG('trigger_dag',
    schedule_interval='@daily',
    default_args=default_args,
    catchup=False) as dag:

    downloading = PythonOperator(
        task_id='downloading',
        python_callable=_downloading
    )
    target=TriggerDagRunOperator(task_id="Trigger",trigger_dag_id="DAG1")
