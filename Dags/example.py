from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago
from airflow.operators.mysql_operator import MySqlOperator
import functools


def python():
   print("Heyyy")

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": days_ago(1),
    "retries": 0,
    "email":["examplem39@gmail.com"],
    "email_on_failure":True,
    "email_on_retry":False}
dag = DAG("example_2", default_args=default_args,catchup=False,schedule_interval='@once')


python=PythonOperator(task_id="python",dag=dag,python_callable=python)
