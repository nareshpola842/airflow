from airflow import DAG
from airflow.operators.subdag import SubDagOperator
from airflow.operators.python_operator import PythonOperator,BranchPythonOperator
from airflow.operators.dummy import DummyOperator
from datetime import datetime
from airflow.utils.dates import days_ago

def python():
   return "PYTHONNN"
def python1():
   return "Airflowwww"
def python2():
   return "Dockerrrrr"


def branch():
   accuracy=5
   if accuracy > 5:
      return 'Python1'
   elif accuracy <= 5:
      return 'Python2'
default_args={'start_date':days_ago(1)}
with DAG("Branching",default_args=default_args,catchup=False,schedule_interval='@once')as dag:
   task=BranchPythonOperator(task_id="Brach",python_callable=branch,dag=dag,do_xcom_push=False)
   task1=PythonOperator(task_id="Python1",python_callable=python,dag=dag)
   task2=PythonOperator(task_id="Python2",python_callable=python1,dag=dag)
   rask3=PythonOperator(task_id="Python3",python_callable=python2,dag=dag,trigger_rule='none_failed_or_skipped')



task >> [task1,task2] >> rask3
