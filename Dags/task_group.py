from airflow import DAG
from airflow.operators.subdag import SubDagOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy import DummyOperator
from datetime import datetime
from airflow.utils.dates import days_ago
from sub import *
from airflow.utils.helpers import chain
from airflow.utils.task_group import TaskGroup

def python():
   print("hello")
def python1():
   print("welcome")
def python3():
   print("to ")
def python4():
   print("python")

default_args={'start_date':days_ago(2)}

with DAG('TaskGroup',schedule_interval=None,default_args=default_args,catchup=False)as dag:
   task=PythonOperator(task_id="task",dag=dag,python_callable=python)
   task3=PythonOperator(task_id="task3",dag=dag,python_callable=python1)
   with TaskGroup("TaskList") as taskgroup:
      task4=PythonOperator(task_id="task4",dag=dag,python_callable=python3)
      with TaskGroup("Tasksublist") as subgroup:
         task5=PythonOperator(task_id="task5",dag=dag,python_callable=python4)

   start=DummyOperator(task_id="Start")
   end=DummyOperator(task_id="end")

start >> task >> taskgroup >> task3 >> end
